from Filosofo import Filosofo
from Garfo import Garfo
from threading import Thread, Lock
from time import time
from random import seed
from termcolor import colored

seed(1001011101010100)

garfos = []
for i in range(500):
    garfos.append(Garfo(True))
    
filosofos = []
for i in range(500):
    if i == 499:
        filosofos.append(Filosofo(str(i), garfos[i], garfos[0]))
    else:
        filosofos.append(Filosofo(str(i), garfos[i], garfos[i+1]))


threads = []

inicio = time()

for filosofo in filosofos:
    print(filosofo.nome, '\testa vivo')
    x = Thread(target=filosofo.viver, args=())
    threads.append(x)
    x.start()

for thread in threads:
    thread.join()

fim = time()

print('\nTempo de execucao: ',  (fim - inicio))
print('\n', colored('Filosofo', 'cyan'), '\t\t\t', colored('Refeicoes', 'cyan'))
for filosofo in filosofos:
    print(filosofo.nome, '\t\t\t', filosofo.quantidade_refeicoes)
