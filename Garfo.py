from threading import Lock
from time import sleep

class Garfo:

    def __init__(self, disponivel):
        self.disponivel = disponivel
        self.mutex = Lock()

    def pegar(self):
        sleep(0.05)
        self.mutex.acquire()
        try:
            if(self.disponivel):
                self.disponivel = False
                self.mutex.release()
                return True
            else:
                self.mutex.release()
                return False
        except:
            self.mutex.release()
    
    def soltar(self):
        self.disponivel = True
