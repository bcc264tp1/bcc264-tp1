from time import sleep
from random import randint
from termcolor import colored

from Garfo import Garfo

class Filosofo:
    def __init__(self, nome, garfo_esquerdo, garfo_direito):
        self.nome = nome
        self.garfo_esquerdo = garfo_esquerdo
        self.garfo_direito = garfo_direito
        self.quantidade_refeicoes = 0

    def refeicoes(self):
        return 5

    def pensar(self):
        print(self.nome, colored('\testá pensando', 'yellow'))
        sleep(randint(0,6))
        print(self.nome, colored('\testá com fome', 'magenta'))

    def comer(self):
        if(self.garfo_direito.pegar()):
            if(self.garfo_esquerdo.pegar()):
                print(self.nome, colored('\testá comendo', 'green'))
                sleep(randint(0,4))
                self.quantidade_refeicoes += 1
                self.garfo_direito.soltar()
                self.garfo_esquerdo.soltar()
            else:
                self.garfo_direito.soltar()
    
    def viver(self):
        while(self.quantidade_refeicoes < self.refeicoes()):
            self.pensar()
            self.comer()
   

        


    
